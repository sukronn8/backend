<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use \Illuminate\Http\Request;
use \App\Models\Attend;
use \App\Models\User;
use Firebase\JWT\JWT;

class AttendController extends BaseController
{
    public function attend(Request $request){
        $token = $request->bearerToken('Authorization');
        $credentials = JWT::decode($token, env('JWT'), ['HS256']);
        $idEvent = $request->input('id_event');
        $attend = new Attend;
        $attend->id_user=$credentials->id;
        $attend->id_event=$idEvent;
        $status=$attend->save();
        if ($status) {
            return response()->json([
                'status'=>true,
                'message'=>'berhasil Join'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'tidak berhasil join'
            ]);
        }
    }
    public function listJoin(Request $request){
        $token = $request->bearerToken('Authorization');
        $credentials = JWT::decode($token, env('JWT'), ['HS256']);
        $data = User::with('attendance')->where('id', $credentials->id)->get();
        return response()->json([
            'status'=>true,
            'data'=>$data
        ]);
    }
    public function index(Request $request){
        $data =User::with('attendance')->get();
        return response()->json([
            'status'=>true,
            'data'=>$data
        ]);
    }
}
