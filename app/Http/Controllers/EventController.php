<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use \Illuminate\Http\Request;
use \App\Models\Event;


class EventController extends BaseController
{
    public function create(Request $request){
        $name = $request->input('name');
        $maxAttends = $request->input('max_attends');
        $location = $request->input('location');
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');
        $event = new Event;
        $event->name=$name;
        $event->max_attends=$maxAttends;
        $event->location=$location;
        $event->start_date=$startDate;
        $event->end_date=$endDate;
        $status=$event->save();
        if ($status) {
            return response()->json([
                'status'=>true,
                'message'=>'Event berhasil didaftarkan'
            ]);
        } else {
            return response()->json([
                'status'=>false,
                'message'=>'Event tidak berhasil didaftarkan'
            ]);
        }
    }
    public function index(Request $request){
        $data =Event::all();
        return response()->json([
            'status'=>true,
            'data'=>$data
        ]);
    }
    public function eventAttendance(){
        $data=Event::with('attendance')->get();
        return response()->json([
            'status'=>true,
            'data'=>$data
        ]);
    }
}
