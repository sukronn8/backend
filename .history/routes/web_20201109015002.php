<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post('/api/register', 'UserController@register');
$router->post('/api/login', 'UserController@login');
$router->post('/api/event', 'EventController@create');
$router->get('/api/event', 'EventController@index');
$router->get('/api/event/attendance', 'EventController@eventAttendance');
$router->post('/api/attend', 'AttendController@attend');
$router->get('/api/attend/all', 'AttendController@index');
$router->get('/api/attend', 'AttendController@listJoin');


