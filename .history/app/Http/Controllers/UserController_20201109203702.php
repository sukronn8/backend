<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use \Illuminate\Http\Request;
use App\Models\User;
use Firebase\JWT\JWT;

class UserController extends BaseController
{
    public function register(Request $request){
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $password = $request->input('password');
        $idRoles = $request->input('id_roles');
        $user = new User;
        $user->name=$name;
        $user->email=$email;
        $user->phone=$phone;
        $user->password=md5($password);
        $user->id_roles=$idRoles;
        $status=$user->save();
        if ($status) {
            return response()->json([
                'status'=>true,
                'message'=>'User berhasil didaftarkan'
            ]);
        } else {
            return response()->json([
                'status'=>false,
                'message'=>'User tidak berhasil didaftarkan'
            ]);
        }
    }
    public function login(Request $request){
        $email = $request->input('email');
        $password = md5($request->input('password'));
        $user = User::where([
            ['email',$email],
            ['password',$password]
        ])->first();
        if (isset($user)) {
            return response()->json([
                'status'=>true,
                'message'=>'berhasil login',
                'token'=>$this->jwt($user)
            ]);
        } else {
            return response()->json([
                'status'=>false,
                'message'=>'tidak berhasil login'
            ]);
        }
    }

    public function profile(Request $request) {
        $token = $request->bearerToken('Authorization');
        $credentials = JWT::decode($token, env('JWT'), ['HS256']);
        $data = User::with('user')->where('id', $credentials->id)->get();
        return response()->json([
            'status' => true,
            'data' => $data
        ]);
    }

    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt",
            'id' => $user->id,
            'iat' => time(),
            'exp' => time() + 60*60*60
        ];


        return 'Bearer '.JWT::encode($payload, env('JWT'));
    }
}
